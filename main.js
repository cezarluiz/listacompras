/**
 * Apps
 */
var ComprasApp = angular
			.module('ComprasApp', ['ngAnimate', 'ngTouch'])
			.run(function () {
				// 300 ms mobile click
				FastClick.attach(document.body);
			});

/**
 * Controllers
 */
ComprasApp.controller('ComprasController', function($scope) {

	$scope.produtos = [];

	$scope.valorTotal = 0;

	// Escopos de configuracao
	$scope.app = {};
	$scope.app.add = false;

	// Clicar
	$scope.adicionaProduto = function() {
		$scope.produtos.push({
			'nome': $scope.produto.nome,
			'valor': ($scope.produto.valor) ? $scope.produto.valor : 0,
			'comprado': false
		});

		// Reseta escopo
		$scope.addProduto.$setPristine();
		$scope.produto = {};
	}

	// Remover produto
	$scope.removerProduto = function(p) {
		$scope.produtos.splice($scope.produtos.indexOf(p), 1);
	}

	// Alterar valor final com mudanças no escopo de produtos
	$scope.$watchCollection('produtos', function (newValue, oldValue) {
		var count = newValue.length,
			valor = 0;

		for (var i = 0; i < count; i++) {
			var active = newValue[i];

			valor += active.valor;
		}

		$scope.valorTotal = valor;
	});
});